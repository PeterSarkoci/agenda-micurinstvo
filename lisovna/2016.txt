== 2016.05.01 ==

Pozorujem, ze aj napriek ucinnym lepovym pasom, listy ceresne na spickach
letoroastov kolonizuju vosky. Nie tak dramaticky ako po minule roky ale
predsa. Cacanska lepotica je vsak v pohode a ostatne stromy tiez.

== 2016.05.07 ==

Zarobeny postrek 1ml Calypso do 2.5L vody a vystriekane primarne na ceresnu
ale nasledne aj na vsetky ostatne ovocne stromy, isabelu, ribezle a egres.
Ochranna doba dva tyzdne (do 2016.05.21).

== 2016.09.13 ==

Poznamky o odrodach dla tohorocneho vysadboveho planu

 2 -

Tato: stredne velke

 3 -



 4 -

Tato: dve z troch mizeria

 5 -

Tato: ozruta js, zalistkuje js

 6 - ina ceresnova paradajka. Tato je super. Plody su balonikovito
pretiahnute. Netyka sa jej ani jeden z problemov pozorovanych pri odrode 10.

Tato: pomensie, neskoro kvitnu

 7 -

Tato: stredne velke, jedna zahynula

 8 -

Tato: dve velke, jedna bez kvetu, stredne velke

 9 -

Tato: stredne velka

10 - ceresnova paradajka dost mizernej kvality. V jednom kuse puka. Kym nie je
puknuta, nejde od konarika. Ked ide od konarika je uz puknuta. Ak si da clovek
zalezat a zbiera ju v stave tesne pred plnou zrelostou, ale kym este nie je
puknuta... pukne mu v chladnicke. Bobula puka pozdlz niektoreho poludnika,
puklina nasledne hnije a plesnivie.

Tato: ozruta, zalistkuje

X1 -

Tato: dost velka

X2 -

Tato: mala

X3 - 

Tato: vysoka 

p2 - brutalne silna paprika. Omylom som kusok prehltol a skoro som zdochol. Na
druhej strane, prijemne som po tom ozivol.

PC - dobra sladka paprika, len mensie plody. Da sa jest aj zelena, ale ak ju
clovek necha na koreni, zreje az do cervena. Plody su mozno pokrkvane trosku
viac, nez by sa slusilo.
